import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import pull_requests from './src/routes/pullRequests.route'

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// The routes for three pull requests apis
app.use('/api/closed/pull/requests', pull_requests);

app.get('/', (req, res) => {
    res.send('Welcome to centurylink github pull request server!!!');
});

app.listen(3003);