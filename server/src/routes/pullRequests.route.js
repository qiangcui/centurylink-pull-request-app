import express from "express";
import pullRequestsController from "../controllers/pullRequests.controller"

const router = express.Router();

router.get('/list', async (req, res) => {
    try {
        await pullRequestsController.getList(req, res);
    } catch (error) {
        console.log(error);
    }
});

// Get comments and commits info with urls from closed pull
router.get('/info', async (req, res) => {
    try {
        await pullRequestsController.getInfoByUrl(req, res);
    } catch (error) {
        console.log(error);
    }
});

export default router;