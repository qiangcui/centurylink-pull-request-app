import JSON from 'circular-json';
import Octokit from '@octokit/rest';
import throttle from '@octokit/plugin-throttling';
Octokit.plugin(throttle);

const controller = {};

const octokit = new Octokit({
    auth: 'token 5db85e34cb9433cc92b9d7472e98ae070f217ec1',
    // Limit the api call to github api server to avoid rate limit error.
    throttle: {
        onRateLimit: (retryAfter, options) => {
            console.warn(`Request quota exhausted for request ${options.method} ${options.url}`);

            if (options.request.retryCount === 0) { // only retries once
                console.log(`Retrying after ${retryAfter} seconds!`);
                return true
            }
        },
        onAbuseLimit: (retryAfter, options) => {
            // does not retry, only logs a warning
            console.warn(`Abuse detected for request ${options.method} ${options.url}`);
        }
    }
});

controller.getList = async (req, res) => {

    let pull_requests_list = [];
    for await (const response of octokit.paginate.iterator('GET /repos/'+ req.query.repo +'/pulls?state=closed')) {
        response.data.forEach((tmp_data) => {
            const repo_id = tmp_data.url.split('/').pop();
            pull_requests_list.push({
                'repo_id': repo_id,
                'title': tmp_data.title,
                'comments_url': tmp_data.comments_url,
                'commits_url': tmp_data.commits_url,
                'created_at': tmp_data.created_at,
                'url': tmp_data.url,
                'user_login': tmp_data.user.login,
            });
        });
    }
    res.send(JSON.stringify(pull_requests_list));
};


controller.getInfoByUrl = async (req, res) => {
    let new_info = [];
    const list = req.query.url.split('/');
    const repo_id = list[list.length - 2];

    await octokit.request('GET ' + req.query.url).then((info) => {

        if (info.data.length !== 0) {
            info.data.forEach((item) => {
                if (item.commit) {
                    new_info.push({
                        'repo_id': repo_id,
                        'name': item.commit.author.name,
                        'email': item.commit.author.email,
                        'date': item.commit.author.date,
                        'message': item.commit.message,
                        'comment_count': item.commit.comment_count
                    });
                } else {
                    new_info.push({
                        'repo_id': repo_id,
                        'name': item.user.login,
                        'comment': item.body,
                        'updated_at': item.updated_at,
                    });
                }
            });
            res.send(JSON.stringify(new_info));
        }
        else {
            res.send([]);
        }
    }).catch((error) => {
        console.log(error);
    });
};

export default controller;