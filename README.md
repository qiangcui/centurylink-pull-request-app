# centurylink-pull-request-app

Get the list of closed pull requests from GitHub API and display with React.

How to run the application?
1. Go to client folder and run ```npm install && npm start``` in the terminal.
2. Go to server folder and run ```npm install && npm start``` in the terminal.
3. Open the browser and hit the localhost:3000

Maintainer: Qiang (Paul) Cui 

Note: Please use npm start instead of docker compose file to run the application.

