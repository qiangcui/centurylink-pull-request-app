import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware } from "redux";
import { Provider } from 'react-redux';
import { Router, Switch, Route } from "react-router-dom";
// import { ConnectedRouter } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk';
import rootReducer from './redux/rootReducer';
import { createBrowserHistory } from 'history';
import 'semantic-ui-css/semantic.css';

const store = createStore(rootReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

const history = createBrowserHistory({ basename: '/' });

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Switch>
                <Route path="/" component={App}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
