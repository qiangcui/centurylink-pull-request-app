import { combineReducers } from "redux";
import listReducer from './reducers/listReducer';
import commitsCommentsReducer from './reducers/commitsCommentsReducer';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
   list: listReducer,
   commitsComments: commitsCommentsReducer,
});