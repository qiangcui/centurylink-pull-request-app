import axios from 'axios';

export const API_SERVER = axios.create();
export const fetchList = (url) => (
    API_SERVER.get(url).then(res => res.data).catch(error => error.response)
);