import { fetchList } from './api';

export const updateList = (new_list) => {
    return {
        type: "UPDATE_LIST",
        payload: {
            list_data: new_list
        }
    }
};

export const updateCommits = (newCommits) => {
    return {
        type: "UPDATE_COMMITS",
        payload: newCommits
    }
};

export const updateComments = (newComments) => {
    return {
        type: "UPDATE_COMMENTS",
        payload: newComments
    }
};

export const removeLists = () => {
    return {
        type: "DELETE_COMMITS",
        payload: null
    }
};

export const removeCommits = () => {
    return {
        type: "DELETE_COMMITS",
        payload: null
    }
};

export const removeComments = () => {
    return {
        type: "DELETE_COMMITS",
        payload: null
    }
};

export const setLoadingTrue = () => {
    return {
        type: "SET_LOADING_TRUE"
    }
};

export const setLoadingFalse = () => {
    return {
        type: "SET_LOADING_FALSE"
    }
};

export const setInfoLoadingTrue = () => {
    return {
        type: "SET_INFO_LOADING_TRUE"
    }
};

export const setInfoLoadingFalse = () => {
    return {
        type: "SET_INFO_LOADING_FALSE"
    }
};

export function getPullRequestsList(url) {
    return dispatch => {

        dispatch(setLoadingTrue());

        const repo = url.split('com/')[1].trim();
        const new_url = '/api/closed/pull/requests/list?repo=' + repo;
        fetchList(new_url)
            .then(
                res => {
                    dispatch(setLoadingFalse());
                    dispatch(updateList(res));
                }
            )
            .catch(
                error => {
                    dispatch(setLoadingFalse());
                }
            )
    }
}

export function getInfo(url) {
    return dispatch => {

        dispatch(setInfoLoadingTrue());

        const new_url = '/api/closed/pull/requests/info?url=' + url;
        const key = new_url.split('/').pop();
        fetchList(new_url)
            .then(
                res => {
                    dispatch(setInfoLoadingFalse());
                    if (key === "commits")
                        dispatch(updateCommits(res));
                    else
                        dispatch(updateComments(res));
                }
            )
            .catch(
                error => {
                    dispatch(setInfoLoadingFalse());
                }
            )
    }
}