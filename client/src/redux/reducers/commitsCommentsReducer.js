const initialState = {
    commits: [],
    comments: [],
    isLoading: false
};

const commitsCommentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_COMMITS":
            const new_commits = action.payload.filter((item) => {
               const matching_list = state.commits.filter((tmp) => tmp.repo_id === item.repo_id);
               return matching_list.length === 0;
            });

            return {
                ...state,
                commits: [...state.commits, ...new_commits]
            };
        case "CLEAR_COMMITS":
            return {
                ...state,
                commits: []
            };
        case "UPDATE_COMMENTS":
            const new_comments = action.payload.filter((item) => {
                const matching_list = state.commits.filter((tmp) => tmp.repo_id === item.repo_id);
                return matching_list.length === 0;
            });

            return {
                ...state,
                comments: [...state.comments, ...new_comments]
            };
        case "CLEAR_COMMENTS":
            return {
                ...state,
                comments: []
            };
        case "SET_INFO_LOADING_FALSE":
            return {
                ...state,
                isLoading: false
            };
        case "SET_INFO_LOADING_TRUE":
            return {
                ...state,
                isLoading: true
            };
        default:
            return state;
    }
};

export default commitsCommentsReducer;
