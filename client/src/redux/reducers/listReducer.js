const initialState = {
    list: [],
    isLoading: false
};

const listReducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_LIST":
            return {
                ...state,
                list: [...action.payload.list_data]
            };
        case "CLEAR_LIST":
            return {
                ...state,
                email_data: []
            };
        case "SET_LOADING_FALSE":
            return {
                ...state,
                isLoading: false
            };
        case "SET_LOADING_TRUE":
            return {
                ...state,
                isLoading: true
            };
        default:
            return state;
    }
};

export default listReducer;