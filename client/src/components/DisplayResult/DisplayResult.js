import React, {Component} from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import DisplayResultItem from './DisplayResultItem';
import './DisplayResult.css';
import { List, Loader, Header } from 'semantic-ui-react'

class DisplayResult extends Component {
    clickHandler = (e, value) => {
        e.preventDefault();
        this.props.handleGetInfo(value.comments_url);
        this.props.handleGetInfo(value.commits_url);
    };

    render() {
        return (
            <div>
                {
                    this.props.isLoading
                        ? ( <div>
                            <Loader active inline='centered'>Loading...</Loader>
                            </div> )
                        : (this.props.list_data.map((value, index) => (
                            <ExpansionPanel key={index} className="email_panel">
                                <ExpansionPanelSummary onClick={(e) => this.clickHandler(e, value)} expandIcon={<ExpandMoreIcon />}>
                                    <Header as='h4' style={{ padding: '10px'}}>
                                        <React.Fragment>
                                        <List style={{textAlign: 'left'}}>
                                            <List.Item><b>Title:</b> {value.title}</List.Item>
                                            <List.Item><b>Repo ID:</b> {value.repo_id}</List.Item>
                                            <List.Item><b>URL:</b> {value.url}</List.Item>
                                            <List.Item><b>User:</b> {value.user}</List.Item>
                                            <List.Item><b>Created At:</b> {value.created_at}</List.Item>
                                            <List.Item><b>Comments Url:</b> {value.comments_url}</List.Item>
                                            <List.Item><b>Commits Url:</b> {value.commits_url}</List.Item>
                                        </List>
                                        </React.Fragment>
                                    </Header>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <DisplayResultItem
                                        repo_item={value}
                                        comments_data={this.props.comments_data}
                                        commits_data={this.props.commits_data}
                                        isInfoLoading={this.props.isInfoLoading}
                                    />
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        )
                        ))
                }
            </div>
        );
    }
}

export default DisplayResult;