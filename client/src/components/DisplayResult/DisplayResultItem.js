import React from 'react';
import {List, Divider, Header, Loader} from 'semantic-ui-react';
import propTypes from 'prop-types';

const DisplayResultItem = ({ repo_item, comments_data, commits_data, isInfoLoading }) => {
    return (
        <List bulleted style={{ textAlign: 'left'}}>
            {/*{ isInfoLoading && <div><Loader active inline='centered'>Loading...</Loader></div>}*/}
            {
                !isInfoLoading && commits_data.filter((commit) => commit.repo_id === repo_item.repo_id)
                    .map((commit, index) => (
                        <React.Fragment key={index}>
                            <Header>Commits {index + 1}</Header>
                            <List.Item>Repo Id: {commit.repo_id}</List.Item>
                            <List.Item>Name: {commit.name}</List.Item>
                            <List.Item>Email: {commit.email}</List.Item>
                            <List.Item>Date: {commit.date}</List.Item>
                            <List.Item>Message: {commit.message}</List.Item>
                            <List.Item>Comment Counts: {commit.comment_count}</List.Item>
                            <Divider />
                        </React.Fragment>
                    ))
            }
            {
                !isInfoLoading && comments_data.filter((comments) => comments.repo_id === repo_item.repo_id)
                    .map((comments, index) => (
                        <React.Fragment key={index}>
                            <Header>Comments {index + 1}</Header>
                            <List.Item>Repo Id: {comments.repo_id}</List.Item>
                            <List.Item>Name: {comments.name}</List.Item>
                            <List.Item>Comments: {comments.comment}</List.Item>
                            <List.Item>Email: {comments.updated_at}</List.Item>
                            <Divider />
                        </React.Fragment>
                    ))
            }
        </List>
    );
};

DisplayResultItem.propTypes = {
    submit: propTypes.object
};

export default DisplayResultItem;