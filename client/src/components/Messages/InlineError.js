import React from 'react';

const InlineError = ({ text }) => (
    <span style={{ color: "red", marginTop: "5px", display: "block" }}>{text}</span>
);

export default InlineError;
