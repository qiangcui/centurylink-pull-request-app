import React from 'react';
import { Form, Header, Button } from "semantic-ui-react";
import InlineError from '../Messages/InlineError';

class InputForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            url: '',
            isWrongValue: false
        }
    }

    handleInput = (e) => {
        this.setState({
            url: e.target.value
        })
    };

    onHandleUpload = () => {
        this.setState({
            isWrongValue: this.validator(this.state.url)
        }, () => {
            if (!this.state.isWrongValue) {
                this.props.handleUpload(this.state.url);
            }
        })
    };

    validator = (url) => {
        return !url.includes('github.com/');
    };

    render() {
        const { isWrongValue } = this.state;
        return (
            <div>
                <Header>Please insert the github repository url here!!!</Header>
                <Form onSubmit={this.onHandleUpload} style={{ padding: '0 50px' }}>
                    <Form.Field>
                        <input
                            type={"text"}
                            name={"github_repo_url"}
                            ref={this.props.inputField}
                            onChange={this.handleInput}
                        />
                        { isWrongValue && <InlineError text={"Please insert the correct github url."}/>}
                        <Button primary style={{ marginTop: '20px'}}>Get List of Closed Pull Requests</Button>
                    </Form.Field>
                </Form>
            </div>
        );
    }
}

export default InputForm;
