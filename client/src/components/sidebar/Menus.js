import React from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';

const menuItems = [
    { label: 'Input Form', item: 'input', url: 'input' },
    { label: 'Test Setup', item: 'output', url: 'output' },
];

class Menus extends React.Component {
    render() {
        const { location } = this.props;
        let activeItem = null;
        if (location && location.pathname) {
            activeItem = location.pathname.substring(1);
        }
        return menuItems.map((menuItem, index) => (
            <Menu.Item
                link
                name={menuItem.item}
                onClick={e => this.props.handleClick(e, menuItem.url)}
                fitted={'horizontally'}
                active={activeItem === menuItem.item}
                data-menu={menuItem.label}
                key={index}
            >
                {menuItem.label}
            </Menu.Item>
        ));
    }
}

const mapStateToProps = state => ({
    location: state.routing.location,
});

export default connect(mapStateToProps)(Menus);
