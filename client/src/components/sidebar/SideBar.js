import React, {Component} from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Sidebar, Menu, Segment } from 'semantic-ui-react';
import AppRoutes from './Router';
import AppMenu from './Menus';

class SideBar extends Component {

    handleItemClick= (e, url) => {
        e.preventDefault();
        if (url) {
            this.props.history.push(`${url}`);
        }
    };

    render() {
        return (
            <Sidebar.Pushable as={Segment} className="AppSidebar">
                <Sidebar
                    as={Menu}
                    borderless
                    animation="push"
                    icon="labeled"
                    inverted
                    vertical
                    width="thin"
                >
                    <AppMenu handleItemClick={this.handleItemClick} />
                </Sidebar>
                <Sidebar.Pusher>
                    <div className="container">
                        <AppRoutes />
                    </div>
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
}

function mapStateToProps(state) {
    return {};
}

export default withRouter(connect(
    mapStateToProps,
)(SideBar));
