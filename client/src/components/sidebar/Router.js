import { Switch, Route, Redirect } from "react-router-dom";
import React, {Component} from 'react';
import InputForm from '../InputForm/InputForm';
import DisplayResult from '../DisplayResult/DisplayResult';

class Routes extends Component {
    render() {
        return (
            <Switch key={this.props.url}>
                <Route exact path="/input" component={InputForm} />
                <Route exact path="/output" component={DisplayResult} />
                <Redirect exact from="/" to="/input" />
            </Switch>
        );
    }
}

export default Routes;