import React, { Component } from 'react';
import './App.css';
import { connect } from "react-redux";
import DisplayResult from './components/DisplayResult/DisplayResult';
import InputForm from './components/InputForm/InputForm';
import { Divider } from "semantic-ui-react";
import { getPullRequestsList, getInfo } from './redux/actions/actions';

class App extends Component {
  constructor(props) {
    super(props);

    this.inputField = React.createRef();
  }

  handleUpload = (url) => {
    this.props.getPullRequestsList(url);
  };

  handleGetInfo = (url) => {
    this.props.getInfo(url);
  };

  render() {
    return (
      <div className="App">
        <InputForm handleInput={this.handleInput} handleUpload={this.handleUpload} inputField={this.inputField}/>
        <Divider />
        <DisplayResult
            list_data={this.props.list_data}
            isLoading={this.props.isLoading}
            handleGetInfo={this.handleGetInfo}
            commits_data={this.props.commits_data}
            comments_data={this.props.comments_data}
            isInfoLoading={this.props.isInfoLoading}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  list_data: state.list.list,
  commits_data: state.commitsComments.commits,
  comments_data: state.commitsComments.comments,
  isLoading: state.list.isLoading,
  isInfoLoading: state.commitsComments.isLoading
});

const mapActionsToProps = {
  getPullRequestsList: getPullRequestsList,
  getInfo: getInfo,
};

export default connect(
    mapStateToProps, mapActionsToProps
)(App);

